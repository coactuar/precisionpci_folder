-- phpMyAdmin SQL Dump
-- version 4.6.6deb5ubuntu0.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jan 22, 2022 at 03:27 PM
-- Server version: 5.7.36-0ubuntu0.18.04.1
-- PHP Version: 7.2.24-0ubuntu0.18.04.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `precisionpci`
--

-- --------------------------------------------------------

--
-- Table structure for table `faculty`
--

CREATE TABLE `faculty` (
  `id` int(11) NOT NULL,
  `user_name` varchar(200) NOT NULL,
  `user_email` varchar(200) NOT NULL,
  `city` varchar(100) NOT NULL,
  `hospital` varchar(100) NOT NULL,
  `speciality` varchar(100) DEFAULT NULL,
  `reg_no` varchar(100) DEFAULT NULL,
  `joining_date` varchar(100) NOT NULL,
  `login_date` date DEFAULT NULL,
  `logout_date` date DEFAULT NULL,
  `logout_status` int(11) NOT NULL DEFAULT '0' COMMENT '1 = logged in',
  `eventname` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `faculty`
--

INSERT INTO `faculty` (`id`, `user_name`, `user_email`, `city`, `hospital`, `speciality`, `reg_no`, `joining_date`, `login_date`, `logout_date`, `logout_status`, `eventname`) VALUES
(1, 'neeraj', 'neeraj@coact.co.in', 'nellore', '9700467764', NULL, NULL, '2021/04/27 15:50:59', '2021-04-27', '2021-04-27', 1, 'Abbott'),
(2, 'Akshat Jharia', 'akshatjharia@gmail.com', 'PUNE', 'COACT', NULL, NULL, '2021/04/27 17:25:13', '2021-04-27', '2021-04-27', 1, 'Abbott'),
(3, 'Sridhara G', 'sridharag@hotmail.com', 'Bangalore ', 'Manipal', NULL, NULL, '2021/04/28 07:41:27', '2021-04-28', '2021-04-28', 1, 'Abbott'),
(4, 'Dr Sridhara G', 'sridharag@hotmail.com', 'Bangalore ', 'Manipal', NULL, NULL, '2021/04/28 07:42:32', '2021-04-28', '2021-04-28', 1, 'Abbott'),
(5, 'neeraj', 'neeraj@coact.co.in', 'neeraj', 'bng', NULL, NULL, '2021/04/28 10:34:45', '2021-04-28', '2021-04-28', 1, 'Abbott'),
(6, 'Akshat Jharia', 'akshatjharia@gmail.com', 'Bangalore', 'COACT', NULL, NULL, '2021/04/28 13:54:53', '2021-04-28', '2021-04-28', 1, 'Abbott'),
(7, 'Dr Sridhara G', 'sridharag@hotmail.com', 'Bangalore', 'Manipal', NULL, NULL, '2021/04/28 15:26:18', '2021-04-28', '2021-04-28', 1, 'Abbott'),
(8, 'Kalyan Goswami', 'kalyan.goswami@abbott.com', 'Bangalore', 'AV', NULL, NULL, '2021/04/28 18:04:10', '2021-04-28', '2021-04-28', 1, 'Abbott'),
(9, 'Udit Mathur', 'udit_m.in@jtbap.com', 'Test', 'Test', NULL, NULL, '2021/04/28 18:17:05', '2021-04-28', '2021-04-28', 1, 'Abbott'),
(10, 'Udit Mathur', 'udit_m.in@jtbap.com', 'Test', 'Test', NULL, NULL, '2021/04/28 18:20:02', '2021-04-28', '2021-04-28', 1, 'Abbott'),
(11, 'Akshat Jharia', 'akshatjharia@gmail.com', 'Bangalore', 'COACT', NULL, NULL, '2021/04/28 18:20:03', '2021-04-28', '2021-04-28', 1, 'Abbott'),
(12, 'Udit Mathur', 'udit.mathur144@jtb-india.com', 'Test', 'Test', NULL, NULL, '2021/04/28 18:21:56', '2021-04-28', '2021-04-28', 1, 'Abbott'),
(13, 'Subhabrata Banerjee', 'subhabrata.banerjee@abbott.com', 'Kolkata ', 'AV ', NULL, NULL, '2021/04/28 18:28:33', '2021-04-28', '2021-04-28', 1, 'Abbott'),
(14, 'Uday Khanolkar', 'udaykhanolkar2002@yahoo.co.in', 'Bangalore', 'Narayana Hrudayalaya', NULL, NULL, '2021/04/28 18:46:42', '2021-04-28', '2021-04-28', 1, 'Abbott'),
(15, 'prabhu halkati', 'drprabhuhalkati@rediff.com', 'belgavi', 'dhakashata', NULL, NULL, '2021/04/28 18:48:22', '2021-04-28', '2021-04-28', 1, 'Abbott'),
(16, 'Dr Sridhara G', 'sridharag@hotmail.com', 'Bangalore', 'Manipal Hospital', NULL, NULL, '2021/04/28 18:54:48', '2021-04-28', '2021-04-28', 1, 'Abbott'),
(17, 'abhijit', 'abhijitvilaskulkarni@yahoo.com', 'bangalore', 'Apollo Hospital', NULL, NULL, '2021/04/28 18:56:28', '2021-04-28', '2021-04-28', 1, 'Abbott'),
(18, 'Dr D S Chadha', 'agiamu@gmail.com', 'BANGALORE', 'Manipal Hospital ', NULL, NULL, '2021/04/28 18:57:15', '2021-04-28', '2021-04-28', 1, 'Abbott'),
(19, 'Kuntal Bhattacharya', 'drkuntal@gmail.com', 'KOLKATA', 'RNTagore ', NULL, NULL, '2021/04/28 18:59:09', '2021-04-28', '2021-04-28', 1, 'Abbott'),
(20, 'Tom Devasia', 'tomdevasia@hotmail.com', 'Manipal', 'KMC Manipal', NULL, NULL, '2021/04/28 19:03:01', '2021-04-28', '2021-04-28', 1, 'Abbott'),
(21, 'Dr Girish B Navasundi', 'navasundi@rediffmail.com', 'Bengaluru', 'Apollo ', NULL, NULL, '2021/04/28 19:06:16', '2021-04-28', '2021-04-28', 1, 'Abbott'),
(22, 'Girish B Navasundi', 'navasundi@rediffmail.com', 'Bangalore', 'apollo hospital, bannerghatta road', NULL, NULL, '2021/04/28 19:41:33', '2021-04-28', '2021-04-28', 1, 'Abbott'),
(23, 'Girish B Navasundi', 'navasundi@rediffmail.com', 'Bangalore', 'apollo hospital, bannerghatta road', NULL, NULL, '2021/04/28 20:35:45', '2021-04-28', '2021-04-28', 1, 'Abbott'),
(24, 'Abhilash', 'abhilash.mohanan@abbott.com', 'Bangalore', 'Abbott', NULL, NULL, '2021/04/28 21:11:25', '2021-04-28', '2021-04-28', 1, 'Abbott');

-- --------------------------------------------------------

--
-- Table structure for table `faculty_new`
--

CREATE TABLE `faculty_new` (
  `id` int(11) NOT NULL,
  `Name` varchar(200) NOT NULL,
  `EmailID` varchar(200) NOT NULL,
  `City` varchar(200) NOT NULL,
  `Hospital` varchar(200) NOT NULL,
  `Last Login On` varchar(200) NOT NULL,
  `Logout Times` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `faculty_new`
--

INSERT INTO `faculty_new` (`id`, `Name`, `EmailID`, `City`, `Hospital`, `Last Login On`, `Logout Times`) VALUES
(1, 'Full Name', 'Email ID', 'Hospital', '', 'In Time', 'Out Time'),
(2, 'Goswami, Kalyan', 'kalyan.goswami@abbott.com', 'AV', '', '4/28/2021, 6:30:31 PM', '4/28/2021, 9:31:46 PM'),
(3, 'Dr Uday Khanolkar (Guest)', 'udaykhanolkar2002@yahoo.co.in', 'Narayana Hrudayalaya', '', '4/28/2021, 6:47:33 PM', '4/28/2021, 9:31:41 PM'),
(4, 'prabhu halkati (Guest)', 'drprabhuhalkati@rediff.com', 'dhakashata', '', '4/28/2021, 6:49:17 PM', '4/28/2021, 9:31:44 PM'),
(5, 'Dr Sridhara G (Guest)', 'sridharag@hotmail.com', 'Manipal Hospital', '', '4/28/2021, 6:56:27 PM', '4/28/2021, 8:42:23 PM'),
(6, 'Dr D S Chadha (Guest)', 'agiamu@gmail.com', 'Manipal Hospital ', '', '4/28/2021, 6:58:03 PM', '4/28/2021, 9:31:39 PM'),
(7, 'KEDAR  KULKARNI', 'abhijitvilaskulkarni@yahoo.com', 'Apollo Hospital', '', '4/28/2021, 6:59:08 PM', '4/28/2021, 9:32:14 PM'),
(8, 'Dr.Kuntal Bhattacharya (Guest)', 'drkuntal@gmail.com', 'RNTagore ', '', '4/28/2021, 6:59:32 PM', '4/28/2021, 9:08:14 PM'),
(9, 'Tom  Devasia [ [MAHE-KMC]', 'tomdevasia@hotmail.com', 'KMC Manipal', '', '4/28/2021, 7:03:20 PM', '4/28/2021, 9:31:43 PM'),
(10, 'Girish Navasundi (Guest)', 'navasundi@rediffmail.com', 'apollo hospital, bannerghatta road', '', '4/28/2021, 7:06:33 PM', '4/28/2021, 8:38:22 PM'),
(11, 'Dr Girish B Navasundi (Guest)', 'navasundi@rediffmail.com', 'apollo hospital, bannerghatta road', '', '4/28/2021, 7:41:59 PM', '4/28/2021, 7:42:02 PM'),
(12, 'Dr Girish B Navasundi (Guest)', 'navasundi@rediffmail.com', 'apollo hospital, bannerghatta road', '', '4/28/2021, 7:51:01 PM', '4/28/2021, 8:54:10 PM'),
(13, 'Dr Girish B Navasundi (Guest)', 'navasundi@rediffmail.com', 'apollo hospital, bannerghatta road', '', '4/28/2021, 8:36:07 PM', '4/28/2021, 9:32:12 PM'),
(14, 'Mohanan, Abhilash', 'abhilash.mohanan@abbott.com', 'Abbott', '', '4/28/2021, 9:13:54 PM', '4/28/2021, 9:31:59 PM');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_questions`
--

CREATE TABLE `tbl_questions` (
  `id` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_email` varchar(255) NOT NULL,
  `user_question` varchar(500) NOT NULL,
  `asked_at` datetime NOT NULL,
  `eventname` varchar(255) NOT NULL,
  `speaker` int(11) NOT NULL DEFAULT '0',
  `answered` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_questions`
--

INSERT INTO `tbl_questions` (`id`, `user_name`, `user_email`, `user_question`, `asked_at`, `eventname`, `speaker`, `answered`) VALUES
(1, 'neeraj', 'neeraj@coact.co.in', 'hello', '2021-04-27 15:50:39', 'Abbott', 0, 0),
(2, 'Sunil Kumar S', 'sunilbmc98@gmail.com', 'Dr Sridhar case. Was ffr repeated after PCI to pda?? If so. What was the value??', '2021-04-28 20:38:12', 'Abbott', 0, 0),
(3, 'Sudhakar Rao', 'msudhakar88@gmail.com', 'This was an excellent case demonstrating the drawback of angiographic guided revascularisation sir', '2021-04-28 21:06:46', 'Abbott', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE `tbl_user` (
  `id` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_email` varchar(500) NOT NULL,
  `city` varchar(100) NOT NULL,
  `hospital` varchar(100) NOT NULL,
  `speciality` varchar(100) DEFAULT NULL,
  `reg_no` varchar(50) DEFAULT NULL,
  `joining_date` datetime NOT NULL,
  `login_date` datetime DEFAULT NULL,
  `logout_date` datetime DEFAULT NULL,
  `logout_status` int(11) NOT NULL DEFAULT '0' COMMENT '1=loggedin',
  `eventname` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE `tbl_users` (
  `id` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_email` varchar(500) NOT NULL,
  `city` varchar(100) NOT NULL,
  `hospital` varchar(100) NOT NULL,
  `speciality` varchar(100) DEFAULT NULL,
  `reg_no` varchar(50) DEFAULT NULL,
  `joining_date` datetime NOT NULL,
  `login_date` datetime DEFAULT NULL,
  `logout_date` datetime DEFAULT NULL,
  `logout_status` int(11) NOT NULL DEFAULT '0' COMMENT '1=loggedin',
  `eventname` varchar(255) NOT NULL,
  `token` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`id`, `user_name`, `user_email`, `city`, `hospital`, `speciality`, `reg_no`, `joining_date`, `login_date`, `logout_date`, `logout_status`, `eventname`, `token`) VALUES
(1, 'neeraj', 'neeraj@coact.co.in', 'nellore', '9700467764', NULL, NULL, '2021-04-27 15:50:34', '2021-04-27 15:50:34', '2021-04-27 17:20:34', 0, 'Abbott', '8c36e3e97c5c6a862fa821ed31d1d830'),
(2, 'Akshat Jharia', 'akshatjharia@gmail.com', 'PUNE', 'COACT', NULL, NULL, '2021-04-27 16:01:04', '2021-04-27 16:01:04', '2021-04-27 17:31:04', 0, 'Abbott', '787f5ad7abc47368216535e194963921'),
(3, 'Kalyan', 'Kalyan.goswami@abbott.com', 'Bangalore ', 'AV ', NULL, NULL, '2021-04-27 18:40:04', '2021-04-27 18:40:04', '2021-04-27 20:10:04', 0, 'Abbott', 'c08bcda974b5b658da7e492f22531437'),
(4, 'chaudappa ', 'Shakapurcj@gmail.com', 'DHARWAD', 'sdmnh dharwad ', NULL, NULL, '2021-04-28 00:00:49', '2021-04-28 00:00:49', '2021-04-28 01:30:49', 0, 'Abbott', '6058feea64f377a2061baa1529a69d0a'),
(5, 'Akshat Jharia', 'akshatjharia@gmail.com', 'Bangalore', 'COACT', NULL, NULL, '2021-04-28 10:28:24', '2021-04-28 10:28:24', '2021-04-28 11:58:24', 0, 'Abbott', '9895919d71e4bd5a902fc9d81aecbe02'),
(6, 'Sayyid Mohammed ', 'drkhilar@hotmail.com', 'Mangaluru', 'Father muller Medical college ', NULL, NULL, '2021-04-28 10:40:09', '2021-04-28 10:40:09', '2021-04-28 12:10:09', 0, 'Abbott', 'c1b045f0cf911eccd6db8f564df945f7'),
(7, 'Akshat Jharia', 'akshatjharia@gmail.com', 'Bangalore', 'COACT', NULL, NULL, '2021-04-28 11:01:28', '2021-04-28 11:01:28', '2021-04-28 12:31:28', 0, 'Abbott', 'c090138c27917604dc057297d7959727'),
(8, 'Akshat Jharia', 'akshatjharia@gmail.com', 'Bangalore', 'COACT', NULL, NULL, '2021-04-28 16:57:45', '2021-04-28 16:57:45', '2021-04-28 18:12:59', 0, 'Abbott', 'fd796ffb677a6cb77178d9b19187883f'),
(9, 'mitthu nayak', 'mitthu.nayak@yahoo.com', 'Berhampur', 'MKCG MEDICAL COLLEGE AND HOSPITAL', NULL, NULL, '2021-04-28 17:22:58', '2021-04-28 17:22:58', '2021-04-28 18:52:58', 0, 'Abbott', '4ef421fce61aa1d3b578f271dbf2e01a'),
(10, 'Venkatesh BP', 'vendvg@gmail.com', 'Davanagere', 'City central hospital ', NULL, NULL, '2021-04-28 18:32:56', '2021-04-28 18:32:56', '2021-04-28 20:02:56', 0, 'Abbott', 'e2d8a86369bb6a8cea66dc75f3251740'),
(11, 'Akshat Jharia', 'akshatjharia@gmail.com', 'Bangalore', 'COACT', NULL, NULL, '2021-04-28 18:52:25', '2021-04-28 18:52:25', '2021-04-28 20:22:25', 0, 'Abbott', '0d5523ee50d661a7b32b4b7708783a8e'),
(12, 'Muhammed Nizar A', 'muhammed.nizar@abbott.com', 'Bangalore', 'Abbott', NULL, NULL, '2021-04-28 18:54:19', '2021-04-28 18:54:19', '2021-04-28 20:24:19', 0, 'Abbott', '00a4b49bb2d865e9145baa68dda2f8f1'),
(13, 'Jagadesh Kalathil', 'jagadeesh.kalathil@abbott.com', 'Bangalore East', 'AV', NULL, NULL, '2021-04-28 18:58:07', '2021-04-28 18:58:07', '2021-04-28 20:28:07', 0, 'Abbott', 'c6cf299ead0a32e14e32ed4b1c035671'),
(14, 'Mahendra ', 'mahendrakumar.g@abbott.com', 'Mangaluru', 'Abbott', NULL, NULL, '2021-04-28 18:58:10', '2021-04-28 18:58:10', '2021-04-28 20:28:10', 0, 'Abbott', 'e90138b1a2e8ec2a3b301dd2701ceda6'),
(15, 'Abhilash', 'abhilash.mohanan@abbott.com', 'Bangalore', 'Abbott', NULL, NULL, '2021-04-28 18:58:58', '2021-04-28 18:58:58', '2021-04-28 20:28:58', 0, 'Abbott', 'c9c1d4ebe371bb4e5f00ad78a5fcaf47'),
(16, 'Atanu Banerjee ', 'atanu.banerjee@abbott.com', 'Kolkata', 'Av', NULL, NULL, '2021-04-28 19:00:05', '2021-04-28 19:00:05', '2021-04-28 20:30:05', 0, 'Abbott', '69a0dddc7b8946ff578893f0f9c33843'),
(17, 'ARUN SRINIVAS', 'arunsrinivas18@gmail.com ', 'MYSORE', 'APOLLO BGS HOSPITALS', NULL, NULL, '2021-04-28 19:00:30', '2021-04-28 19:00:30', '2021-04-28 20:30:30', 0, 'Abbott', '3168a8230feec78ba9dc32021aa26199'),
(18, 'Arun James', 'arun.nelluvelil@abbott.com', 'BANGALORE', 'AV', NULL, NULL, '2021-04-28 19:01:47', '2021-04-28 19:01:47', '2021-04-28 20:31:47', 0, 'Abbott', 'a44d78b4f1751d10835beb73f75779c7'),
(19, 'Nuthan', 'nuthanmohite@gmail.com', 'Bangalore', 'Apollo', NULL, NULL, '2021-04-28 19:03:15', '2021-04-28 19:03:15', '2021-04-28 20:33:15', 0, 'Abbott', '982269219eedc72e944a2b2c2559a7d4'),
(20, 'Nagamani AC', 'drnagamani_c@yahoo.co.in', 'Bengaluru', 'Jayadeva', NULL, NULL, '2021-04-28 19:03:28', '2021-04-28 19:03:28', '2021-04-28 19:05:55', 0, 'Abbott', 'b773d0416bb3c1719ef474d2852c9aa9'),
(21, 'Sunil Roy', 'sunil.roy@abbott.com', 'Bhubaneswar', 'AV', NULL, NULL, '2021-04-28 19:03:37', '2021-04-28 19:03:37', '2021-04-28 20:33:37', 0, 'Abbott', '61319898f5817a16cfe3619d89b61b4a'),
(22, 'Keshavamurthy ', 'keshavamurthyg@gmail.com', 'New Delhi ', 'AHRR', NULL, NULL, '2021-04-28 19:04:17', '2021-04-28 19:04:17', '2021-04-28 20:34:17', 0, 'Abbott', '55372b9e3671020ebacf4dde067ceb82'),
(23, 'Sreedhara ', 'kummas82@gmail.com', 'Mysore ', 'SJICR ', NULL, NULL, '2021-04-28 19:04:43', '2021-04-28 19:04:43', '2021-04-28 20:34:43', 0, 'Abbott', '400c1671a1cf002dd84458b3379de8ba'),
(24, 'Aj swamy', 'ajayswamy@rediffmail.com', 'Bangalore ', 'Command hospital ', NULL, NULL, '2021-04-28 19:05:01', '2021-04-28 19:05:01', '2021-04-28 20:35:01', 0, 'Abbott', '9357d36dc4048f7cf8a0a6eada7b36e7'),
(25, 'dr.keshavamurthy.v', 'drkeshav05@gmail.com', 'mysore', 'nh mysore', NULL, NULL, '2021-04-28 19:05:13', '2021-04-28 19:05:13', '2021-04-28 20:35:13', 0, 'Abbott', '0e0972a132bdb15c52ba9c4da203959b'),
(26, 'Srinivas P', 'drpsrinivas@gmail.com', 'Mysore ', 'NH', NULL, NULL, '2021-04-28 19:05:50', '2021-04-28 19:05:50', '2021-04-28 20:35:50', 0, 'Abbott', 'f2577d13777ee75f1bb141ee23dce132'),
(27, 'Arun James', 'arun.nelluvelil@abbott.com', 'Bangalore', 'AV', NULL, NULL, '2021-04-28 19:06:38', '2021-04-28 19:06:38', '2021-04-28 20:36:38', 0, 'Abbott', 'ba3248c0af36f528fd21b94cfa182854'),
(28, 'jayashree', 'khargej@gmail.com', 'Bangalore', 'SJICSR', NULL, NULL, '2021-04-28 19:07:35', '2021-04-28 19:07:35', '2021-04-28 20:37:35', 0, 'Abbott', '0889575248d315a51bca7e887f6c0db3'),
(29, 'Sunil wani ', 'zapsunil@hotmail.com', 'Mumbai ', 'Kdah', NULL, NULL, '2021-04-28 19:07:58', '2021-04-28 19:07:58', '2021-04-28 20:37:58', 0, 'Abbott', 'd6989b4273bee6d40da03978c2b681e3'),
(30, 'Nagamani A C', 'drnagamani_c@yahoo.co.in', 'Bengaluru', 'Jayadeva hospital ', NULL, NULL, '2021-04-28 19:08:09', '2021-04-28 19:08:09', '2021-04-28 20:38:09', 0, 'Abbott', 'f49f89491ce3698c43c880ac8ef96214'),
(31, 'mathaivaluchira mathai', 'mathaivaluchira@gmail.com', 'alleppy', 'Bmjh', NULL, NULL, '2021-04-28 19:08:23', '2021-04-28 19:08:23', '2021-04-28 20:38:23', 0, 'Abbott', '7d59ff3c9c282b73efe13d4cb5c76e7c'),
(32, 'Sunil Roy', 'sunil.roy@abbott.com', 'Bhubaneswar', 'AV', NULL, NULL, '2021-04-28 19:09:01', '2021-04-28 19:09:01', '2021-04-28 20:39:01', 0, 'Abbott', '852fd4f6f6417024ed504fd93564fa45'),
(33, 'Chirag Parikh', 'parikhcj@hotmail.com', 'Mumbai', 'S r mehta cardiac institute ', NULL, NULL, '2021-04-28 19:09:08', '2021-04-28 19:09:08', '2021-04-28 20:39:08', 0, 'Abbott', 'a912d9f5ba6515a049f9f0636a740dd1'),
(34, 'Sreedhara R', 'kummas82@gmail.com', 'Mysore ', 'SJICR ', NULL, NULL, '2021-04-28 19:09:39', '2021-04-28 19:09:39', '2021-04-28 20:39:39', 0, 'Abbott', '784ef54c89735b85f82186ba8518cf1a'),
(35, 'Venketesulu', 'rsv_vhcb@yahoo.com', 'Bangalore ', 'Astercmi ', NULL, NULL, '2021-04-28 19:09:47', '2021-04-28 19:09:47', '2021-04-28 20:39:47', 0, 'Abbott', '253b03b26983d9066630b1c1ed1ae666'),
(36, 'Guru Prasad H P', 'guruprasadhp@yahoo.co.in', 'Mysore', 'Bgs Apollo hospitals Mysore', NULL, NULL, '2021-04-28 19:10:25', '2021-04-28 19:10:25', '2021-04-28 20:40:25', 0, 'Abbott', '15d0e1cfc277babf5b49833cd6218220'),
(37, 'Dr Asfaque Ahmed ', 'ahmed.asfaque@gmail.com', 'Kolkata', 'Apollo Kolkata ', NULL, NULL, '2021-04-28 19:10:57', '2021-04-28 19:10:57', '2021-04-28 20:40:57', 0, 'Abbott', 'df3d1b31f0f4497529d49c6f76ff8dc9'),
(38, 'Dr Aritra Konar ', 'konar1978.dr@gmail.com', 'Kolkata', 'AB', NULL, NULL, '2021-04-28 19:11:26', '2021-04-28 19:11:26', '2021-04-28 20:41:26', 0, 'Abbott', '7451ccd65fa4fd2464d524ebf9cf0ae7'),
(39, 'Bala', 'bala.subramani@gmail.com', 'Kolkata', 'Apollo', NULL, NULL, '2021-04-28 19:11:48', '2021-04-28 19:11:48', '2021-04-28 20:41:48', 0, 'Abbott', '33f4f2f996457deed538988dc69f0f37'),
(40, 'K r Shyamsunder ', 'krs.sunder@gmail.com', 'Bangalore ', 'Sagar hospital ', NULL, NULL, '2021-04-28 19:12:07', '2021-04-28 19:12:07', '2021-04-28 20:42:07', 0, 'Abbott', '23ec719d804b216a2dec37815a8a19a4'),
(41, 'Dr Arijit Ghosh ', 'arijitjoy@gmail.com', 'Kolkata', 'AMRI Salt Lake ', NULL, NULL, '2021-04-28 19:12:14', '2021-04-28 19:12:14', '2021-04-28 20:42:14', 0, 'Abbott', '45fa0ceb95abde21840a55aa8386cf52'),
(42, 'Santanu Bag', 'bag.santanu@yahoo.com', 'Kolkata', 'Apollo Gleneagles ', NULL, NULL, '2021-04-28 19:12:38', '2021-04-28 19:12:38', '2021-04-28 20:42:38', 0, 'Abbott', '80c995d71406d1592425207f9df84414'),
(43, 'Gopal Raorane', 'gopalrane_2008@yahoo.com', 'Mumbai', 'AV', NULL, NULL, '2021-04-28 19:13:11', '2021-04-28 19:13:11', '2021-04-28 20:43:11', 0, 'Abbott', 'fd9f4c9bd2206a9e9d86b4bd03b408be'),
(44, 'Subhabrata Banerjee', 'subhabrata.banerjee@abbott.com', 'Kolkata ', 'AV ', NULL, NULL, '2021-04-28 19:13:34', '2021-04-28 19:13:34', '2021-04-28 20:43:34', 0, 'Abbott', 'b2646f003c3d1387df9a6777322c3ef4'),
(45, 'Dr Joy Sanyal ', 'drjoysanyal@gmail.com', 'Durgapur ', 'Healthworld ', NULL, NULL, '2021-04-28 19:13:39', '2021-04-28 19:13:39', '2021-04-28 20:43:39', 0, 'Abbott', '1907c36cc7ca7bb018316cc2d65181b8'),
(46, 'Shyam G', 'g.shyam@abbott.com', 'Bangalore', 'Abbott', NULL, NULL, '2021-04-28 19:13:53', '2021-04-28 19:13:53', '2021-04-28 20:43:53', 0, 'Abbott', 'fb7ddc04aa34c9157e9e2aae29010b97'),
(47, 'Dr Sachit Majumder ', 'Sachit.majumder@gmail.com', 'Kolkata', 'Apollo Gleneagles ', NULL, NULL, '2021-04-28 19:14:10', '2021-04-28 19:14:10', '2021-04-28 20:44:10', 0, 'Abbott', 'ba75c3c0194c7f5971d3b565ee3cfc2a'),
(48, 'Udit Mathur', 'udit_m.in@jtbap.com', 'Test', 'Test', NULL, NULL, '2021-04-28 19:14:46', '2021-04-28 19:14:46', '2021-04-28 20:44:46', 0, 'Abbott', '2429e6f174915dbd3e11754baca565fc'),
(49, 'Dr S S Das ', 'sankha.suvra@gmail.com', 'Kolkata', 'Apollo Kolkata ', NULL, NULL, '2021-04-28 19:14:56', '2021-04-28 19:14:56', '2021-04-28 20:44:56', 0, 'Abbott', '8e30888fcfb6d817ca938bb544919366'),
(50, 'Shouvik Bhattacharya', 'shouvik.bhattacharya@abbott.com', 'Kolkata', 'AV', NULL, NULL, '2021-04-28 19:15:16', '2021-04-28 19:15:16', '2021-04-28 20:45:16', 0, 'Abbott', 'e8ea876dcf6980e8b6927687ffcdc036'),
(51, 'Dr H Ray', 'drhnray@gmail.com', 'Kolkata', 'AMRI Salt Lake ', NULL, NULL, '2021-04-28 19:15:47', '2021-04-28 19:15:47', '2021-04-28 20:45:47', 0, 'Abbott', 'e0c50e76575d3df972013939fc73ad7a'),
(52, 'Dr Swapan Kumar Dey ', 'dey.swapan@gmail.com', 'Kolkata', 'Apollo Kolkata ', NULL, NULL, '2021-04-28 19:16:17', '2021-04-28 19:16:17', '2021-04-28 20:46:17', 0, 'Abbott', '60fc2386f7fa16a707a8b7b01c6111ec'),
(53, 'Saleha Noorain', 'salehanoorain@gmail.com', 'Bangalore', 'Jayadeva', NULL, NULL, '2021-04-28 19:16:37', '2021-04-28 19:16:37', '2021-04-28 20:46:37', 0, 'Abbott', '051c83438a6212fd0804d54500aa4958'),
(54, 'Nagamani AC', 'drnagamani_c@yahoo.co.in', 'Bengaluru', 'Jayadeva', NULL, NULL, '2021-04-28 19:17:23', '2021-04-28 19:17:23', '2021-04-28 20:47:23', 0, 'Abbott', 'f8475ea35edbac2624d7025bb7a5d407'),
(55, 'Praveen kumar', 'jipmer.praveen@gmail.com', 'kolar', 'Forgot ', NULL, NULL, '2021-04-28 19:18:16', '2021-04-28 19:18:16', '2021-04-28 20:48:16', 0, 'Abbott', '44f8feb881d0c19868043eb6ae4e935e'),
(56, 'Atanu Banerjee ', 'atanu.banerjee@abbott.com', 'Kolkata', 'AV', NULL, NULL, '2021-04-28 19:18:54', '2021-04-28 19:18:54', '2021-04-28 20:48:54', 0, 'Abbott', 'cff15c7f7b281ba503dad73b9e9463b2'),
(57, 'Murali M', 'murali13.manohar@gmail.com', 'Bangalore', 'Bangalore Baptist hospital', NULL, NULL, '2021-04-28 19:19:06', '2021-04-28 19:19:06', '2021-04-28 20:49:06', 0, 'Abbott', '2adee93d71ac48a1efdc70d24bf9c43b'),
(58, 'Sunil wani ', 'zapsunil@hotmail.com', 'Mumbai', 'Kdah', NULL, NULL, '2021-04-28 19:20:04', '2021-04-28 19:20:04', '2021-04-28 20:50:04', 0, 'Abbott', 'da59808db84ccd591940ab080e5439ac'),
(59, 'Nagamani AC', 'drnagamani_c@yahoo.co.in', 'Bengaluru', 'Jayadeva', NULL, NULL, '2021-04-28 19:20:09', '2021-04-28 19:20:09', '2021-04-28 19:25:30', 0, 'Abbott', '20d953433ea92116f2fc463784aeade1'),
(60, 'Gopal Raorane', 'gopalrane_2008@yahoo.com', 'Mumbai', 'AV', NULL, NULL, '2021-04-28 19:20:39', '2021-04-28 19:20:39', '2021-04-28 20:50:39', 0, 'Abbott', 'a4dbd22772a5c1569cc2c0557f6f83c0'),
(61, 'Dr Darshan', 'drdoc2002@gmail.com', 'Bangalore', 'Manipal bangalore', NULL, NULL, '2021-04-28 19:22:31', '2021-04-28 19:22:31', '2021-04-28 20:52:31', 0, 'Abbott', '6aa724e9b1d991049a08a50c82113fd0'),
(62, 'Krshyamsunder', 'krs.sunder@gmail.com', 'Bangalore ', 'Sagar hospital ', NULL, NULL, '2021-04-28 19:23:08', '2021-04-28 19:23:08', '2021-04-28 20:53:08', 0, 'Abbott', 'b4a0db9ced14c79fc9e9b587a1b97814'),
(63, 'Guru Prasad H P', 'guruprasadhp@yahoo.co.in', 'Mysore', 'Bgs Apollo hospitals Mysore', NULL, NULL, '2021-04-28 19:23:20', '2021-04-28 19:23:20', '2021-04-28 20:53:20', 0, 'Abbott', 'd0cf24ebec36451d9b6228a9544b7129'),
(64, 'Nagamani A C', 'drnagamani_c@yahoo.co.in', 'Bengaluru', 'Jayadeva hospital ', NULL, NULL, '2021-04-28 19:25:02', '2021-04-28 19:25:02', '2021-04-28 19:27:20', 0, 'Abbott', 'bbbd28ac9e4b55b379f13b97d26d31c0'),
(65, 'Sunil Kumar S', 'sunilbmc98@gmail.com', 'Bangalore', 'Columbia Asia Hospital', NULL, NULL, '2021-04-28 19:26:32', '2021-04-28 19:26:32', '2021-04-28 20:56:32', 0, 'Abbott', '8b228bbdcf5db4b0137226d9919cf305'),
(66, 'Naga Srinivaas', 'drakondi.ns@gmail.com', 'Bengaluru', 'Manipal hospital', NULL, NULL, '2021-04-28 19:26:59', '2021-04-28 19:26:59', '2021-04-28 20:56:59', 0, 'Abbott', 'a65534758cc77081cc28993d04c41a0f'),
(67, 'Subhabrata Banerjee', 'subhabrata.banerjee@abbott.com', 'Kolkata ', 'AV ', NULL, NULL, '2021-04-28 19:30:06', '2021-04-28 19:30:06', '2021-04-28 21:00:06', 0, 'Abbott', 'ec58e6f235c8d9c6b03f84c81fde58b0'),
(68, 'Sunil Kumar S', 'sunilbmc98@gmail.com', 'Bangalore', 'Columbia Asia Hospital', NULL, NULL, '2021-04-28 19:32:08', '2021-04-28 19:32:08', '2021-04-28 21:31:58', 0, 'Abbott', '4e0faf4127ed75a6c803905abc101886'),
(69, 'chirag d', 'chiragnichi@gmail.com', 'bangalore', 'fortis', NULL, NULL, '2021-04-28 19:34:35', '2021-04-28 19:34:35', '2021-04-28 21:04:35', 0, 'Abbott', '745924258194d257c137bffecc6c8397'),
(70, 'sridhar', 'drnsridhara@gmail.com', 'Bangalore', 'fortis', NULL, NULL, '2021-04-28 19:39:55', '2021-04-28 19:39:55', '2021-04-28 21:09:55', 0, 'Abbott', 'f6e1fe7ca695463a457d6d7987bf17f6'),
(71, 'Venkatesh BP', 'vendvg@gmail.com', 'Davanagere', 'City hospital ', NULL, NULL, '2021-04-28 19:41:27', '2021-04-28 19:41:27', '2021-04-28 21:11:27', 0, 'Abbott', '1d1d5755aed033896605ee680fdb1857'),
(72, 'Subhabrata Banerjee', 'subhabrata.banerjee@abbott.com', 'Kolkata ', 'AV ', NULL, NULL, '2021-04-28 19:42:48', '2021-04-28 19:42:48', '2021-04-28 21:12:48', 0, 'Abbott', 'adc74a5af884a90f8c875f78fef92336'),
(73, 'Dr Subramanyam ', 'drsubramanyam@rediffmail.com', 'Mangalore', 'K s hegde hospital ', NULL, NULL, '2021-04-28 19:47:13', '2021-04-28 19:47:13', '2021-04-28 21:17:13', 0, 'Abbott', '1141af7a2a63073b875d70d702ef0e5e'),
(74, 'Dr Vithal D Bagi', 'drvithaldbagicardio2015@gmail.com', 'Bangalore', 'Apollo hospitals bannerughatta road Bangalore', NULL, NULL, '2021-04-28 19:48:23', '2021-04-28 19:48:23', '2021-04-28 21:18:23', 0, 'Abbott', '0eae81180d6bf6ea6656662201e6391c'),
(75, 'Dr Vithal D Bagi', 'drvithaldbagicardio2015@gmail.com', 'Bangalore', 'Apollo hospitals bannerughatta road Bangalore', NULL, NULL, '2021-04-28 19:50:24', '2021-04-28 19:50:24', '2021-04-28 21:20:24', 0, 'Abbott', 'c66743668048e2a1e18feceb2dcbebfe'),
(76, 'Dr Vithal D Bagi', 'drvithaldbagicardio2015@gmail.com', 'Bangalore', 'Apollo hospitals bannerughatta road Bangalore', NULL, NULL, '2021-04-28 19:51:46', '2021-04-28 19:51:46', '2021-04-28 21:21:46', 0, 'Abbott', '23d41d1ef75893cb17c21a4f836435a8'),
(77, 'Dr Vithal D Bagi', 'drvithaldbagicardio2015@gmail.com', 'Bangalore', 'Apollo hospitals bannerughatta road Bangalore', NULL, NULL, '2021-04-28 19:52:59', '2021-04-28 19:52:59', '2021-04-28 21:22:59', 0, 'Abbott', 'a0c399f11e7173c44aebe256207067a8'),
(78, 'Dr Vithal D Bagi', 'drvithaldbagicardio2015@gmail.com', 'Bangalore', 'Apollo hospitals bannerughatta road Bangalore', NULL, NULL, '2021-04-28 19:54:17', '2021-04-28 19:54:17', '2021-04-28 21:24:17', 0, 'Abbott', 'd09ee8ad5f2c151897ea332245ee041b'),
(79, 'Kalyan Goswami', 'Kalyan.goswami@abbott.com', 'Bangalore ', 'AV ', NULL, NULL, '2021-04-28 19:57:03', '2021-04-28 19:57:03', '2021-04-28 21:27:03', 0, 'Abbott', '5607d7b813721238f94fe4628f5370ac'),
(80, 'Dr Vithal D Bagi', 'drvithaldbagicardio2015@gmail.com', 'Bangalore', 'Apollo hospitals bannerughatta road Bangalore', NULL, NULL, '2021-04-28 19:58:32', '2021-04-28 19:58:32', '2021-04-28 21:28:32', 0, 'Abbott', '6d0944bd5c59ac4e4f62086a096bd449'),
(81, 'Keshava', 'drkeshavar@gmail.com', 'Bangalore ', 'Fortis ', NULL, NULL, '2021-04-28 20:00:17', '2021-04-28 20:00:17', '2021-04-28 21:30:17', 0, 'Abbott', 'c67298d97a1f6a0f7860b818098018c2'),
(82, 'Dr Vithal D Bagi', 'drvithaldbagicardio2015@gmail.com', 'Bangalore', 'Apollo hospitals bannerughatta road Bangalore', NULL, NULL, '2021-04-28 20:00:38', '2021-04-28 20:00:38', '2021-04-28 21:30:38', 0, 'Abbott', '8b296df3b6254b74ef00f8d251bdd7ee'),
(83, 'Keshava', 'drkeshavar@gmail.com', 'Bangalore ', 'Fortis ', NULL, NULL, '2021-04-28 20:07:13', '2021-04-28 20:07:13', '2021-04-28 21:37:13', 0, 'Abbott', '0601ba06c4679c45067687dd4544e63f'),
(84, 'Suman Kumar Saha', 'sumankumarsaha360@gmail.com', 'Kolkata', 'R.N.Tagore Hospital', NULL, NULL, '2021-04-28 20:08:36', '2021-04-28 20:08:36', '2021-04-28 21:38:36', 0, 'Abbott', '9d7e4a4c3a62a8769135a3bbeb667c99'),
(85, 'Madakari Nayaka', '2020madhu.icare@gmail.com', 'Bengalore ', 'Sparsh', NULL, NULL, '2021-04-28 20:09:24', '2021-04-28 20:09:24', '2021-04-28 21:39:24', 0, 'Abbott', 'f35c7c722e87afca53131da2391deffd'),
(86, 'Keshava', 'drkeshavar@gmail.com', 'Bangalore ', 'Fortis ', NULL, NULL, '2021-04-28 20:11:53', '2021-04-28 20:11:53', '2021-04-28 21:41:53', 0, 'Abbott', '522622dd398cade28d3ac09a4ba8cfe3'),
(87, 'Suman Kumar Saha', 'sumankumarsaha360@gmail.com', 'Kolkata', 'R.N.Tagore Hospital', NULL, NULL, '2021-04-28 20:13:23', '2021-04-28 20:13:23', '2021-04-28 21:43:23', 0, 'Abbott', 'd54b04508b45e7a2646644d782c7aa6b'),
(88, 'Dr Vithal D Bagi', 'drvithaldbagicardio2015@gmail.com', 'Bangalore', 'Apollo hospitals bannerughatta road Bangalore', NULL, NULL, '2021-04-28 20:16:51', '2021-04-28 20:16:51', '2021-04-28 21:46:51', 0, 'Abbott', '831913153cf97c7bd4fd081b8a26c7ce'),
(89, 'HARSHA M M', 'drharshamm@gmail.com', 'Mysore', 'Jayadeva', NULL, NULL, '2021-04-28 20:18:23', '2021-04-28 20:18:23', '2021-04-28 21:48:23', 0, 'Abbott', '8cc695e248aa6d3913a9b7d63f53b5b4'),
(90, 'Senthil Kumar Natarajan ', 'senthil.natarajan@abbott.com', 'Bangalore ', 'Abbott ', NULL, NULL, '2021-04-28 20:18:30', '2021-04-28 20:18:30', '2021-04-28 21:48:30', 0, 'Abbott', '68f753a8415f0982cbe59a9b47a32596'),
(91, 'Subhabrata Banerjee', 'subhabrata.banerjee@abbott.com', 'Kolkata ', 'AV ', NULL, NULL, '2021-04-28 20:18:57', '2021-04-28 20:18:57', '2021-04-28 21:48:57', 0, 'Abbott', 'ebcff922f1d46d9f719ac4a1c4dba7fa'),
(92, 'Madakari Nayaka', '2020madhu.icare@gmail.com', 'Bengalore ', 'Sparsh', NULL, NULL, '2021-04-28 20:22:24', '2021-04-28 20:22:24', '2021-04-28 21:52:24', 0, 'Abbott', '98b4ba92b6938194ab48b3fbc26d38ff'),
(93, 'sagar desai', 'desaisagar13@gmail.com', 'Bagalkot ', 'Hsk', NULL, NULL, '2021-04-28 20:28:18', '2021-04-28 20:28:18', '2021-04-28 21:58:18', 0, 'Abbott', '15bbf7e1a7f3eb4915a6367713a19ec2'),
(94, 'Dr Vithal D Bagi', 'drvithaldbagicardio2015@gmail.com', 'Bangalore', 'Apollo hospitals bannerughatta road Bangalore', NULL, NULL, '2021-04-28 20:30:07', '2021-04-28 20:30:07', '2021-04-28 20:34:25', 0, 'Abbott', '8c18b942c7f0134bb122a64f43d1403e'),
(95, 'Suman Kumar Saha', 'sumankumarsaha360@gmail.com', 'Kolkata', 'R.N.Tagore Hospital', NULL, NULL, '2021-04-28 20:33:13', '2021-04-28 20:33:13', '2021-04-28 22:03:13', 0, 'Abbott', 'd3b62dde78b8f3f5ada5081f5fe127da'),
(96, 'Dr Vithal D Bagi', 'drvithaldbagicardio2015@gmail.com', 'Bangalore', 'Apollo hospitals bannerughatta road Bangalore', NULL, NULL, '2021-04-28 20:34:34', '2021-04-28 20:34:34', '2021-04-28 22:04:34', 0, 'Abbott', '2e7d269569eb617096b6fdeb1f7d1fc8'),
(97, 'Dr Vithal D Bagi', 'drvithaldbagicardio2015@gmail.com', 'Bangalore', 'Apollo hospitals bannerughatta road Bangalore', NULL, NULL, '2021-04-28 20:35:48', '2021-04-28 20:35:48', '2021-04-28 20:37:59', 0, 'Abbott', 'e4c34815f1679bef1aa9b8b7bf7d374b'),
(98, 'Dr Vithal D Bagi', 'drvithaldbagicardio2015@gmail.com', 'Bangalore', 'Apollo hospitals bannerughatta road Bangalore', NULL, NULL, '2021-04-28 20:38:08', '2021-04-28 20:38:08', '2021-04-28 22:08:08', 0, 'Abbott', '19cde478f95b571622f211fef9c8a3a6'),
(99, 'Suman Kumar Saha', 'sumankumarsaha360@gmail.com', 'Kolkata', 'R.N.Tagore Hospital', NULL, NULL, '2021-04-28 20:41:54', '2021-04-28 20:41:54', '2021-04-28 20:55:01', 0, 'Abbott', 'e47393e7531b7990b3d5d744cc7e6729'),
(100, 'Sudhakar Rao', 'msudhakar88@gmail.com', 'Manipal', 'Knc', NULL, NULL, '2021-04-28 20:46:34', '2021-04-28 20:46:34', '2021-04-28 21:31:00', 0, 'Abbott', 'd6608df352e04805236c45f401e4cc30'),
(101, 'Sudeep', 'sudeepachar.124@gmail.com', 'Bangalore', 'Apollo', NULL, NULL, '2021-04-28 20:48:28', '2021-04-28 20:48:28', '2021-04-28 22:18:28', 0, 'Abbott', 'a0a4989de9b26f1f59533e409c11ec7d'),
(102, 'Subhabrata Banerjee', 'subhabrata.banerjee@abbott.com', 'Kolkata ', 'AV ', NULL, NULL, '2021-04-28 20:54:16', '2021-04-28 20:54:16', '2021-04-28 22:24:16', 0, 'Abbott', 'cb96568b41bd4225eae7180c00f35ea5'),
(103, 'Aniketh Vijay', 'aniketh.vijay7@gmail.com', 'Chikmagalur ', 'Ashraya heart institute ', NULL, NULL, '2021-04-28 21:03:38', '2021-04-28 21:03:38', '2021-04-28 22:33:38', 0, 'Abbott', '3a3210773381f4a3c2ae0618632c282e'),
(104, 'Senthil Kumar Natarajan ', 'senthil.natarajan@abbott.com', 'Bangalore ', 'Abbott ', NULL, NULL, '2021-04-28 21:27:56', '2021-04-28 21:27:56', '2021-04-28 22:57:56', 0, 'Abbott', 'cce5bded1683814b035ac6bd10b1ef0f'),
(105, 'Jinto vj', 'jintovj2008@gmail.com', 'Banglore ', 'Banglore hospital', NULL, NULL, '2021-04-28 22:43:55', '2021-04-28 22:43:55', '2021-04-29 00:13:55', 0, 'Abbott', '9ca6b0d56b34d9cea523fdbb9ad65c32'),
(106, 'Saurabh Chopra', 'saurabhchopra235@gmail.com', 'Gurgaon', 'Medanta', NULL, NULL, '2021-04-30 20:15:38', '2021-04-30 20:15:38', '2021-04-30 21:45:38', 1, 'Abbott', 'ea9951f52d4002aedcb2940243efcd85'),
(107, 'Keshavamurthy CB', 'cbkeshavamurthy@rediffmail.com', 'Mysore', 'Columbia Asia hospital', NULL, NULL, '2021-04-30 21:08:53', '2021-04-30 21:08:53', '2021-04-30 22:38:53', 1, 'Abbott', '3b5dd328f8089925d8c62b16ad8a9b8d'),
(108, 'Abhinay', 'abhinay.tibdewal@gmail.com', 'Bengaluru', 'Jayadeva', NULL, NULL, '2021-05-19 20:27:14', '2021-05-19 20:27:14', '2021-05-19 21:57:14', 1, 'Abbott', '8195a8c2c71ad560959e1743d0f26e59'),
(109, 'Sagar Mhadgut', 'sagar.mhadgut@abbott.com', 'Mumbai', 'Abbott', NULL, NULL, '2021-05-31 12:40:22', '2021-05-31 12:40:22', '2021-05-31 12:40:27', 0, 'Abbott', 'f92d9bed5b67a39da230ef68ed996b67');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `faculty`
--
ALTER TABLE `faculty`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `faculty_new`
--
ALTER TABLE `faculty_new`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_questions`
--
ALTER TABLE `tbl_questions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `faculty`
--
ALTER TABLE `faculty`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `faculty_new`
--
ALTER TABLE `faculty_new`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `tbl_questions`
--
ALTER TABLE `tbl_questions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=110;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
